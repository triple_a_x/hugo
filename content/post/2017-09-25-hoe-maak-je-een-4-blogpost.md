---
Title: Week 4 blog
subtitle: via Web of smartgit of Atom
Date: 25-9-2017
Tags: [''blog'', ''blogging'', ''nieuw'', ''pagina'', ''post''
---

# Weblog,

# *Week 4*
### Maandag, 25 September
We zijn begonnen met het afnemen van de interviews. Omdat we tempo moesten maken zijn we in teams gegaan. Een persoon die het de persoon overhoord en een persoon die alles noteert. Helaas zijn we maar met zijn vijven. Dus ik moest dit alleen doen, wat me toch wat extra moeite kostte waardoor ik wel wat langer bezig was dan de rest. Ik heb twee interviews afgenomen van Evelien uit CMD1A en Serhat uit CMD1F (denk ik). Bij mijn interview, schrijf ik steekwoorden op van hun uitleg en verwerk  dit later uitgebreid. Dus ik heb hierna de interviews netjes gemaakt en verwerkt. Nadat ik het had verwerkt, heb ik conclusies gemaakt van beide hun interview. Toen hebben we gezamenlijk alle conclusies van alle interviews besproken. Zo proberen we op één conclusie te komen. We hebben steekwoorden, overeenkomsten van de geïnterviewde gedaan en daarvan een mindmap gemaakt. We hebben met z'n alle plaatjes opgezocht van de steekwoorden zodat we er een visuele conclusie van konden maken die Eva samenstelt. Aan het eind van de dag gaan we bespreken, wat voor beeld we hebben deze week en wat we moeten doen voor deze week. Dus wat ons doel is voor deze week en de taakverdeling.

### 26 September, dinsdag
Gisteren na het hoorcollege, zijn we begonnen met brainstormen over concepten met CCOD techniek. Ik merkte dat mijn groep moeite heeft met ideeën bedenken aan het begin vooral. Dus heb ik wat ideeën voorgelegd, waar ik al over na had gedacht. Maar ik merkte dat ze erna helemaal los kwamen en gewoon een duwtje nodig. We hebben besproken over welke realiseerbaar waren welke niet, welke origineel waren en welke niet en wat nou echt een goed concept begin was. We hebben een top gemaakt van ideeën en die verdeeld over ons team om thuis daarvan een concept uitbreiding van te maken.

#### Dinsdagavond
Ik heb gezeten aan mijn Adobe huiswerk, wat best wat tijd vereist. Erna heb ik een verslag gemaakt over de basis van escaperoom en cluedo. Erna heb ik mijn eigen concept uitgewerkt en online/ offline elementen erin gestopt.

### Woensdag, 27 September
Gestart met studie coaching. Erna hebben we concepten gedeeld met elkaar en al ons concepten uitgewerkt. Erna hebben we gezamenlijk gekeken welke concept het beste was, maar kwamen er niet uit. Dus we hebben feedback gevraagd van twee teams en Jantien. 

