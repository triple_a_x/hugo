Title: Week 6 blog
subtitle: via Web of smartgit of Atom
Date: 09-10-2017
Tags: [''blog'', ''blogging'', ''nieuw'', ''pagina'', ''post''

## Week 6
### Maandag, 9 Oktober
Vandaag is Iteratie 3 ingegaan, we hebben zojuist de presentatie gehad. Deze week moeten we:
*Prototype ontwikkelen
*Feedback oplossen
*Botsingen uit de weg halen
*Starr schrijven
*Adobe huiswerk
*Tentamen leren

We zijn aan de slag gegaan met de ontvangen feedback. Ieder teamlid onderzoekt individueel een creatieve game die overeenkomsten vertoont met ons concept.
Gezamenlijk de feedback op de presentatie bespreken en kijken hoe we bepaalde
punten kunnen verbeteren.

### Woensdag, 12 Oktober
Vandaag hebben we nagedacht over een prototype. Het onderzoek besproken en het concept verbeterd aan de
hand van de ontvangen feedback en onderzoeksresultaten. We hebben nagedacht over het testen van het prototypen en de expo. Er is ook een taakverdeling opgesteld.

# Vakantie - week 7

